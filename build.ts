import { resolve } from 'path'

const rootDir = resolve(__dirname)

const outDir = (name: string) => resolve(rootDir, 'dist', name)

const srcDir = resolve(rootDir, 'src')

await Bun.build({
  entrypoints: [srcDir],
  outdir: outDir('app'),
  sourcemap: 'none',
  minify: false,
  target: 'browser',
  define: {
    "nested.boolean": "injected_at_buildtime",
  },
})

console.log('Build complete')