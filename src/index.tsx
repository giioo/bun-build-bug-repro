/// <reference lib="dom" />
/// <reference lib="dom.iterable" />

import { createRoot } from 'react-dom/client'

declare global {
  interface SomeObject {
    boolean: any
  }

  var nested: SomeObject; 
}

// this should be replaced at build time
const testing = nested.boolean

function App() {
  return <div>hello, world</div>
}

const root = document.createElement('div')
root.id = 'chrome-extension-root'
document.body.append(root)

createRoot(root).render(<App />)
